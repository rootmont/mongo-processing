import os


#######
# KEYS
#######
RUNNING_ENV = os.environ.get('RUNNING_ENV', 'local')

DB_NAME = os.environ.get('DB_NAME', 'api_stats')
DB_LOCATION = os.environ.get('DB_LOCATION', 'mysql')
DB_USER = os.environ.get('DB_USER', 'root')
DB_PASSWORD = os.environ.get('DB_PASSWORD', 'IFarmLemons')
DB_PORT = os.environ.get('DB_PORT', '3306')

CMC_API_KEY = os.environ.get('CMC_API_KEY', '')

GITHUB_USER = os.environ.get('GITHUB_USER', '')
GITHUB_PASSWORD = os.environ.get('GITHUB_PASSWORD', '')

INFURA_API_KEY = os.environ.get('INFURA_API_KEY', '')
INFURA_PROJECT_ID = os.environ.get('INFURA_PROJECT_ID', '')
INFURA_SECRET = os.environ.get('INFURA_SECRET', '')

NOMICS_KEY = os.environ.get('NOMICS_KEY', '')

TWITTER_CONSUMER_KEY = os.environ.get('TWITTER_ACCESS_SECRET', '')
TWITTER_CONSUMER_SECRET = os.environ.get('TWITTER_ACCESS_TOKEN', '')
TWITTER_ACCESS_TOKEN = os.environ.get('TWITTER_CONSUMER_KEY', '')
TWITTER_ACCESS_SECRET = os.environ.get('TWITTER_CONSUMER_SECRET', '')

RABBITMQ_HOST = os.environ.get('RABBITMQ_HOST', 'rabbitmq-mgmt')
RABBITMQ_USER = os.environ.get('RABBITMQ_USER', 'guest')
RABBITMQ_PASS = os.environ.get('RABBITMQ_PASS', 'guest')
RABBITMQ_EXCHANGE = os.environ.get('RABBITMQ_EXCHANGE', '')

AMAZON_ID = os.environ.get('AMAZON_ID', '')
AMAZON_KEY = os.environ.get('AMAZON_KEY', '')
AMAZON_REGION = "us-west-1"
BLOCK_BUCKET = "eth-block-store"

########
# Non Env
########
AGE_CLUSTERS = ['Emerging', 'Established', 'Mature']
MARKET_CAP_CLUSTERS = ['Micro Cap', 'Small Cap', 'Medium Cap', 'Large Cap']
INDUSTRY_CLUSTERS = ['Platform', 'Gaming', 'Computing', 'Exchange', 'Payments', 'Blockchain Interoperability',
                     'Asset Backed', 'Enterprise', 'Media', 'Public Services', 'IOT', 'Social', 'Finance', 'Privacy',
                     'Logistics']
SLIDING_WINDOW_SIZES = [7, 30, 90, 180, 360, 720, 10000]

import requests
import re
import logging
import datetime
from typing import Union

from model.coin import Coin
from model.reddit_stats import RedditStats
from service.coin_service import CoinService
from util import retry, get_headers, get_proxy_d
from app import db


class RedditService(object):
    def __init__(self):
        self._reddit_url = "https://www.reddit.com/r/"
        self._session = requests.session()

    @retry(retries=5)
    def get_reddit_info(self, coin: Coin) -> Union[RedditStats, None]:
        stats = RedditStats()
        stats.coin_id = coin.id
        stats.date_time = datetime.datetime.now()
        stats.date = datetime.date.today()

        response = self._session.get(self._reddit_url + coin.reddit, proxies=get_proxy_d(), headers=get_headers())
        a = re.findall(pattern="subscribersCount.*?([0-9]+),", string=response.text, flags=re.DOTALL)
        if len(a) > 0:
            stats.subscribers = int(a[0])
        else:
            logging.warning('Could not find reddit subscribers from subreddit: {}, full url: {}'
                            .format(coin.reddit, self._reddit_url + coin.reddit))
            stats = None

        return stats

    def update_latest_stats(self):
        coin_service = CoinService()
        coins = coin_service.get_coins_local()
        for coin in coins:
            if coin.reddit is not None and coin.reddit is not "":
                reddit_stat = self.get_reddit_info(coin)
                if reddit_stat is not None:
                    db.session.add(reddit_stat)
        db.session.commit()

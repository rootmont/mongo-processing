import twitter
import logging
import datetime
from typing import Union

from config import TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET, TWITTER_ACCESS_TOKEN, TWITTER_ACCESS_SECRET
from model.coin import Coin
from model.twitter_stats import TwitterStats


class TwitterService(object):
    def __init__(self):
        self._twitter_api = twitter.Api(consumer_key=TWITTER_CONSUMER_KEY, consumer_secret=TWITTER_CONSUMER_SECRET,
                                        access_token_key=TWITTER_ACCESS_TOKEN, access_token_secret=TWITTER_ACCESS_SECRET)
        logging.info(self._twitter_api.VerifyCredentials())
        pass

    def get_twitter_user(self, coin: Coin) -> Union[TwitterStats, None]:
        stats = TwitterStats()
        stats.date_time = datetime.datetime.now()
        stats.date = datetime.date.today()
        if coin.twitter is None:
            return None
        try:
            # Not retrying because we are gracefully catching the error here.
            user = self._twitter_api.GetUser(screen_name=coin.twitter, return_json=True)
            stats.tweets = user['statuses_count']
            # likes = user['favourites_count']
            stats.following = int(user['friends_count'])
            stats.followers = int(user['followers_count'])
            if stats.following == 0:
                stats.leadership = float(stats.followers)
            else:
                stats.leadership = float(stats.followers) / float(stats.following)
            stats.coin_id = coin.id
        except Exception as e:
            logging.warning('Could not find twitter account for user "{}"'.format(coin.twitter))
            logging.warning(e)
            stats = None
        return stats

import json
import requests
import datetime
import logging
import time
from typing import Union
from requests.auth import HTTPBasicAuth

from config import GITHUB_USER, GITHUB_PASSWORD
from model.coin import Coin
from model.github_stats import GithubStats
from service.coin_service import CoinService
from util import retry
from app import db


class GithubService(object):
    def __init__(self):
        self._session = requests.session()
        self._github_url = 'https://api.github.com/repos/'

    @retry(retries=5)
    def get_github_info(self, coin: Coin) -> Union[GithubStats, None]:
        if coin.github is None or coin.github is "":
            logging.warning("Github url was None or empty for coin: {}".format(coin.token_name))
            return None

        stat = GithubStats()
        stat.date = datetime.date.today()
        stat.date_time = datetime.datetime.now()
        stat.coin_id = coin.id

        data_url = self._github_url + self._get_github_url_suffix(coin.github)
        res = self._session.get(data_url, auth=HTTPBasicAuth(GITHUB_USER, GITHUB_PASSWORD))

        self._parse_rate_limit(res)

        text = res.text
        data = json.loads(text)
        try:
            if res.status_code == 404:
                logging.warning("Github repo was not found for coin: {}, and url: {}".format(coin.token_name, coin.github))
                return None
            stat.size = int(data.get('size', 0))
            stat.star_gazers = int(data.get('stargazers_count', 0))
            stat.forks = int(data.get('forks_count', 0))
            stat.open_issues = int(data.get('open_issues', 0))
            stat.last_commit = datetime.datetime.strptime(data['pushed_at'].replace("Z", ""), "%Y-%m-%dT%H:%M:%S")
            contributor_data = self._get_contributor_data('%s/contributors' % data_url)
            stat.contributors = int(contributor_data['contributors'])
            stat.contributions = int(contributor_data['contributions'])
        except (KeyError, TypeError, json.JSONDecodeError) as e:
            logging.warning(e)
            return None
        return stat

    def _get_github_url_suffix(self, repo_url):
        parts = repo_url.split('/')
        if parts[-1] == '':
            return '/'.join(parts[-3:-1])
        return '/'.join(parts[-2:])

    @retry(retries=5)
    def _get_contributor_data(self, url: str):
        data = {
            'contributors': 0,
            'contributions': 0
        }
        page = 0
        while True:
            res = self._session.get('%s?page=%d' % (url, page), auth=HTTPBasicAuth(GITHUB_USER, GITHUB_PASSWORD))
            self._parse_rate_limit(res)

            if res.status_code != 200:
                logging.info("Github couldn't access contributors / contributions for url: {} likely because there as none. status_code: {}"
                             .format('%s?page=%d' % (url, page), res.status_code))
                break

            text = res.text
            loaded_json = json.loads(text)
            if len(loaded_json) == 0:
                break
            page += 1
            data['contributors'] += len(loaded_json)
            data['contributions'] += sum([x['contributions'] for x in loaded_json])
        return data

    def _parse_rate_limit(self, res):
        remaining = int(res.headers.get("X-RateLimit-Remaining"))
        reset = int(res.headers.get("X-RateLimit-Reset"))
        if remaining <= 1:
            logging.info("Github rate limit almost reached - waiting {} seconds until next round."
                         .format(reset - int(time.time())))
            if reset - int(time.time()) > 0:
                time.sleep(reset - int(time.time()))
                logging.info("Done waiting for Github API rate limit - resuming.")
                raise Exception("Rate limit exceeded")

    def update_latest_stats(self):
        coin_service = CoinService()
        coins = coin_service.get_coins_local()
        for coin in coins:
            if coin.github is not None and coin.github is not "":
                github_stat = self.get_github_info(coin)
                if github_stat is not None:
                    db.session.add(github_stat)
        db.session.commit()

import logging
import requests
from typing import List, Union

from model.coin import Coin
from util import get_headers, get_proxy_d, retry
from app import db


class CoinService(object):
    def __init__(self):
        self._g_sheet_url = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vTGh4W0Nfq3v-AxXVXDoCNjrIx9Y8B5DO_' + \
                           'nCjJaLlSTYhdV97fZYHfNu2JAPfl0-R_yMH5A3DQk0fn0/pub?gid=0&single=true&output=tsv'

    def update_coin_list(self) -> List[Coin]:
        remote_coins: List[Coin] = self.get_coins_remote()
        db_coins: List[Coin] = self.get_coins_local()
        coin_names_remote: List[str] = list(map(lambda r: r.token_name, remote_coins))
        for db_coin in db_coins:
            if db_coin.token_name not in coin_names_remote:
                db_coin.deleted = True
                db.session.add(db_coin)
        for remote_coin in remote_coins:
            found_db_coin: Union[Coin, None] = next((x for x in db_coins if x.token_name == remote_coin.token_name), None)
            if found_db_coin is not None:
                found_db_coin.update_from_remote(remote_coin)
                db.session.add(found_db_coin)
            else:
                db.session.add(remote_coin)
        db.session.commit()
        return self.get_coins_local()

    def get_coins_local(self) -> List[Coin]:
        return Coin.all()

    @retry(retries=10)
    def get_coins_remote(self) -> List[Coin]:
        logging.info('Acquiring spreadsheet')
        ret = []
        response = requests.get(self._g_sheet_url, headers=get_headers(), proxies=get_proxy_d())
        text = response.text
        for line in text.split('\n')[1:]:
            cols = line.split('\t')
            symbol, name, industry, ico, mineable = cols[:5]
            open_source, ico_raise, max_supply, hash_algorithm, blockchain_type = cols[5:10]
            disclosure, execution, potential, github_url, description = cols[10:15]
            website, whitepaper, contract_addr, twitter, reddit, telegram, _, _, _ = cols[15:]
            website = website.replace('\n', '').replace('\r', '')
            whitepaper = whitepaper.replace('\n', '').replace('\r', '')
            twitter = twitter.replace('\n', '').replace('\r', '')
            reddit = reddit.replace('\n', '').replace('\r', '')
            telegram = telegram.replace('\n', '').replace('\r', '')
            symbol = symbol.strip()
            name = name.strip()
            industry = industry.strip()
            hash_algorithm = hash_algorithm.strip()
            blockchain_type = blockchain_type.strip()
            contract_addr = contract_addr.strip()
            twitter = twitter.strip()
            reddit = reddit.strip()
            telegram = telegram.strip()
            if not ico.isdigit():
                ico = 0
            if not mineable.isdigit():
                mineable = 0
            if not open_source.isdigit():
                open_source = 0
            if not ico_raise.isdigit():
                ico_raise = 0
            if not max_supply.isdigit():
                max_supply = 0
            if not disclosure.isdigit():
                disclosure = 0
            if not execution.isdigit():
                execution = 0
            if not potential.isdigit():
                potential = 0

            coin = Coin()
            coin.description = description
            coin.ticker_symbol = symbol
            coin.company_name = name
            coin.industry = industry
            coin.token_name = name
            coin.ico = ico == 1
            coin.mineable = mineable == 1
            coin.open_source = open_source == 1
            coin.ico_raise = ico_raise
            coin.max_supply = max_supply
            coin.hash_algorithm = hash_algorithm
            coin.block_chain_type = blockchain_type
            coin.execution = execution
            coin.potential = potential
            coin.market_cap_cluster = ""
            coin.age_cluster = ""
            coin.disclosure = disclosure == 1
            coin.website_url = website
            coin.white_paper_url = whitepaper
            coin.twitter = twitter
            coin.reddit = reddit
            coin.telegram = telegram
            coin.github = github_url
            coin.contract_address = contract_addr
            coin.deleted = False
            ret.append(coin)
        return ret

import json
import requests
import logging
from dateutil import parser as dateparser
from datetime import timezone

from model.block_transactions import BlockTransactions
from tasks.block_fetch_work import BlockFetchWork
from tasks.message_sender import MessageSender
from util import retry


class EosBlockService(object):
    def __init__(self):
        self._url = "https://eos.greymass.com/v1/chain/get_info"
        self._block_chain = "eos"

    @retry(retries=5)
    def get_chain_height_remote(self) -> int:
        res = requests.get(self._url)
        if res.text is None:
            raise Exception("Could not fetch EOS remote block height")
        j = json.loads(res.text)
        num_blocks = j['last_irreversible_block_num']
        return num_blocks

    @retry(retries=5)
    def get_block_remote(self, block_num: int) -> BlockTransactions:
        payload = json.dumps({'block_num_or_id': str(block_num)})
        res = requests.post('https://eos.greymass.com/v1/chain/get_block', data=payload)
        if res.status_code != 200:
            raise Exception("Could not fetch EOS remote block")
        data = json.loads(res.text)
        return BlockTransactions(block_chain=self._block_chain,
                                 block_num=block_num,
                                 date_time=dateparser.parse(data['timestamp']),
                                 epoch_second=dateparser.parse(data['timestamp']).replace(tzinfo=timezone.utc).timestamp(),
                                 num_transactions=len(data['transactions']))

    def get_chain_height_local(self) -> int:
        block_transaction = BlockTransactions.get_latest_block(block_chain=self._block_chain)
        if block_transaction is not None:
            return block_transaction.block_num
        return 0

    def update_latest_queue(self):
        # TODO: Need a persistent store for this rather than just blasting this?
        # Maybe a unique constraint --
        latest_block_remote = self.get_chain_height_remote()
        latest_block_local = self.get_chain_height_local()
        for i in range(latest_block_local + 1, latest_block_remote):
            MessageSender.get_instance().send_message(BlockFetchWork.queue, BlockFetchWork(i, self._block_chain))
            if i % 500 == 0:
                logging.info("Sent blockwork message for {}: {} ".format(self._block_chain, str(i)))


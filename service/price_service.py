import requests
import json
import logging
import datetime
from typing import List, Set

from config import NOMICS_KEY
from model.coin import Coin
from model.price_volume import PriceVolume
from service.nomics import Nomics
from util import retry


class PriceService(Nomics):
    def __init__(self):
        super().__init__()
        self._session = requests.session()

    def get_price_for_coins(self, coins: List[Coin], start_date: datetime, end_date: datetime = datetime.datetime.now()) -> List[List[PriceVolume]]:
        ret = []
        for coin in coins:
            ret.append(self.get_price_nomics(coin, start_date, end_date))
        return ret

    @retry(retries=10)
    def get_price_nomics(self, coin: Coin, start_date: datetime, end_date: datetime = datetime.datetime.now()):
        logging.info('Getting price for %s from date %s' % (coin.ticker_symbol, start_date))
        start_date_str = start_date.strftime(self._nomics_date_format) + 'T00:00:00Z'
        end_date_str = end_date.strftime(self._nomics_date_format) + 'T00:00:00Z'

        params = {
            'key': NOMICS_KEY,
            'interval': '1d',
            'currency': coin.ticker_symbol,
            'start': start_date_str,
            'end': end_date_str
        }
        path = 'candles'
        response = self._session.get(url=self._nomics_url + path, params=params)
        if response.status_code != 200:
            raise LookupError('Unable to access nomics api: %s' % (self._nomics_url + path))
        data = json.loads(response.text)

        ret: List[PriceVolume] = []
        for d in data:
            price_volume = PriceVolume()
            price_volume.date = self.parse_nomics_timestamp(d)
            price_volume.price_high = float(d['high'])
            price_volume.price_low = float(d['low'])
            price_volume.price_close = float(d['close'])
            price_volume.price_open = float(d['open'])
            price_volume.trading_volume = int(d['volume'])
            price_volume.coin_id = coin.id
            price_volume.ticker_symbol = coin.ticker_symbol
            ret.append(price_volume)

        return ret

    @retry(retries=5)
    def supply_nomics(self, days_back):
        logging.info('Getting mcaps from nomics for %d days back' % days_back)
        start_date = datetime.date.today() - datetime.timedelta(days_back + 1)
        end_date = datetime.date.today() - datetime.timedelta(days_back)
        start_date_str = start_date.strftime(self._nomics_date_format) + 'T00:00:00Z'
        end_date_str = end_date.strftime(self._nomics_date_format) + 'T00:00:00Z'
        params = {
            'key': NOMICS_KEY,
            'start': start_date_str,
            'end': end_date_str
        }
        url = 'https://api.nomics.com/v1/'
        path = 'supplies/interval'
        response = requests.get(url=url + path, params=params)
        if response.status_code == 401:
            raise LookupError('Unable to access nomics api: %s' % (url + path))
        data = json.loads(response.text)
        ret = {}

        for x in data:
            if x['open_timestamp'] is not None and x['open_available'] is not None:
                open_date = self.parse_nomics_timestamp(x['open_timestamp'])
                if open_date not in ret:
                    ret[open_date] = {}
                ret[open_date][x['currency']] = int(float(x['open_available']))
            if x['close_timestamp'] is not None and x['close_available'] is not None:
                close_date = self.parse_nomics_timestamp(x['close_timestamp']) + datetime.timedelta(1)
                if close_date not in ret:
                    ret[close_date] = {}
                ret[close_date][x['currency']] = int(float(x['close_available']))
        return ret

    def get_nomics_supply_coins(self) -> Set[str]:
        supplies = self.supply_nomics(1)
        date = list(supplies.keys())[0]
        return set(supplies[date].keys())

import requests
import json
import datetime

from config import NOMICS_KEY
from util import retry


class Nomics(object):
    def __init__(self):
        self._nomics_url = 'https://api.nomics.com/v1/'
        self._nomics_date_format = '%Y-%m-%d'

    @retry(retries=5)
    def get_nomics_coins(self):
        params = {'key': NOMICS_KEY}
        response = requests.get(url=self._nomics_url + "currencies", params=params)
        if response.status_code != 200:
            raise LookupError('Unable to access nomics api: {}, status: {}'.format(self._nomics_url, response.status_code))
        data = json.loads(response.text)
        return {x['id'] for x in data}

    def parse_nomics_timestamp(self, datum) -> datetime.date:
        if type(datum) is str:
            datum = {'timestamp': datum}
        dt = datetime.datetime.strptime(datum['timestamp'].split('T')[0], self._nomics_date_format)
        return datetime.date(dt.year, dt.month, dt.day)

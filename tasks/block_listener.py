import pika
import jsonpickle
import logging

from service.eos_block_service import EosBlockService
from tasks.block_fetch_work import BlockFetchWork
from app import db


class BlockListener(object):
    def __init__(self):
        self._connection = pika.BlockingConnection(pika.ConnectionParameters(host='rabbitmq-mgmt'))
        self._channel = self._connection.channel()
        # declare queue
        self._channel.queue_declare(queue=BlockFetchWork.queue, durable=True)
        self._channel.basic_qos(prefetch_count=10)
        self._channel.basic_consume(self.callback, queue=BlockFetchWork.queue)

        self._eos_block = EosBlockService()

    def callback(self, ch, method, properties, body):
        decoded_work: BlockFetchWork = jsonpickle.decode(body)
        if decoded_work.block_chain == "eos":
            block = self._eos_block.get_block_remote(decoded_work.block_num)
            db.session.add(block)
            db.session.commit()
            if decoded_work.block_num % 1000 == 0:
                logging.info("Saved eos block: " + str(decoded_work.block_num))
        ch.basic_ack(delivery_tag=method.delivery_tag)

    def start(self):
        logging.info("Started listening for incoming blocks to fetch")
        self._channel.start_consuming()

import datetime

from tasks.dtos.task_dto_meta import TaskDtoMeta


class EthereumDayStatsWork(metaclass=TaskDtoMeta):
    queue = "ethereum_days"
    """
    Transmits that we are ready to process day stats for a particular stats
    """
    def __init__(self, start_block: int, end_block: int, date: datetime.date):
        self.start_block = start_block
        self.end_block = end_block
        self.date = date



class TaskDtoMeta(type):
    def __new__(cls, name, bases, body):
        if body.get('queue') is None:
            raise TypeError("{} doesn't implement the queue string class variable.".format(name))
        return super().__new__(cls, name, bases, body)

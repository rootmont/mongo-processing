

class BlockFetchWork(object):
    queue = "block_transactions"

    def __init__(self, block_num: int, block_chain: str):
        self.block_num = block_num
        self.block_chain = block_chain

import jsonpickle
import pika
import pika_pool
import json


class MessageSender(object):
    __instance = None

    @staticmethod
    def get_instance():
        """ Static access method. """
        if MessageSender.__instance is None:
            MessageSender()
        return MessageSender.__instance

    def __init__(self):
        """ Virtually private constructor. """
        if MessageSender.__instance is not None:
            raise Exception("Class is a singleton")
        MessageSender.__instance = self

        self._pool = pika_pool.QueuedPool(
            create=lambda: pika.BlockingConnection(pika.ConnectionParameters(host='rabbitmq-mgmt')),
            max_size=10,
            max_overflow=10,
            timeout=10,
            recycle=3600,
            stale=45,
        )

    def send_message(self, queue: str, obj):
        with self._pool.acquire() as cxn:
            cxn.channel.basic_publish(
                body=jsonpickle.encode(obj),
                exchange='',
                routing_key=queue,
                properties=pika.BasicProperties(
                    content_type='application/json',
                    content_encoding='utf-8',
                    delivery_mode=2,
                )
            )

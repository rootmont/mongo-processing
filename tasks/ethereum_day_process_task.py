import concurrent
import pika
import json
import jsonpickle
import logging
import datetime
from typing import List, Union
from concurrent.futures.thread import ThreadPoolExecutor

from app import db
from model.ethereum_block_days import EthereumBlockDays
from model.block_info import BlockInfo
from repository.s3_repository import S3Repository
from tasks.dtos.ethereum_day_stats_work import EthereumDayStatsWork


class EthereumDayProcessTask(object):
    def __init__(self):
        self._connection = pika.BlockingConnection(pika.ConnectionParameters(host='rabbitmq-mgmt'))
        self._channel = self._connection.channel()
        # declare queue
        self._channel.queue_declare(queue=EthereumDayStatsWork.queue, durable=True)
        self._channel.basic_qos(prefetch_count=10)
        self._channel.basic_consume(self.callback, queue=EthereumDayStatsWork.queue)
        self._s3_repository = S3Repository()

    def get_block(self, block_num: int):
        return self._s3_repository.get_block_info(block_num)

    def process_day(self, block_day: EthereumBlockDays):
        day_blocks: List[BlockInfo] = []
        # We are going to store this in the database eventually
        if block_day is not None and block_day.blocks is not None:
            day_blocks = jsonpickle.loads(block_day.blocks)
        else:
            with ThreadPoolExecutor(max_workers=100) as executor:
                num_processed = 0
                start = datetime.datetime.now()
                futures = {executor.submit(self.get_block, x) for x in range(block_day.start_block, block_day.end_block)}
                for future in concurrent.futures.as_completed(futures):
                    current: BlockInfo = future.result()
                    day_blocks.append(current)
                    num_processed += 1
                    if num_processed % 400 == 0:
                        now = datetime.datetime.now()
                        difference: datetime.timedelta = (now - start)
                        blocks_per_sec = num_processed / difference.total_seconds()
                        logging.info("Blocks processed: {} out of {}, b/s: {}, est. min remaining: {}".format(
                            num_processed,
                            (block_day.end_block - current.block_num),
                            round(blocks_per_sec, 2),
                            round((block_day.end_block - current.block_num) / (blocks_per_sec * 60), 2)
                        ))

            # Store this blob for replay-ability, getting here to refresh the session
            block_day = EthereumBlockDays.get_by_date(block_day.day)
            block_day.blocks = json.dumps([block.to_json() for block in day_blocks])
            db.session.add(block_day)
            db.session.commit()
        # For coin in coin that is an ERC 20
        # 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef
        # Add decimal places to coin - only get from local

        # TODO: Parse out all the ERC20 information
        # TODO: Parse out all the tx data for ethereum and save it
        # TODO: Check if it already exists before so we don't double down?
        # TODO: Get the number of decimal places if it's a remote coin

    def callback(self, ch, method, properties, body):
        decoded_work = json.loads(body)
        day_date: datetime.date = datetime.datetime.strptime(decoded_work['day'], '%m/%d/%Y').date()
        block_day: Union[EthereumBlockDays, None] = EthereumBlockDays.get_by_date(day_date)

        if block_day is None:
            logging.error("block_day was none for date {}, though we got a message to process.".format(decoded_work['day']))
            return

        logging.info("Grabbing day {} from {} to {}".format(decoded_work['day'],
                                                            decoded_work['start_block'],
                                                            decoded_work['end_block'] + 1))

        self.process_day(block_day)

        # TODO: Error handling
        ch.basic_ack(delivery_tag=method.delivery_tag)

    def start(self):
        logging.info("Started listening for incoming blocks to fetch")
        self._channel.start_consuming()

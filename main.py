import logging
import datetime
from app import application
import argparse

from model.price_volume import PriceVolume
from service.eos_block_service import EosBlockService
from service.coin_service import CoinService
from service.price_service import PriceService
from service.social.github_service import GithubService
from service.social.reddit_service import RedditService
from tasks.block_listener import BlockListener
from tasks.ethereum_day_process_task import EthereumDayProcessTask
from util import insert_update


class NoRunningFilter(logging.Filter):
    def filter(self, record):
        return "maximum number of running" not in record.msg


logging.basicConfig(level=logging.INFO)
appschedule_filter = NoRunningFilter()
logging.getLogger("apscheduler.scheduler").addFilter(appschedule_filter)

if __name__ == '__main__':
    CoinService().update_coin_list()
    coins = CoinService().get_coins_local()

    EthereumDayProcessTask().start()
    # twitter_service = TwitterService()
    #for coin in coins:
        # if coin.twitter is not None:
        #    twitter_stat = twitter_service.get_twitter_user(coin)
        #    logging.info(twitter_stat)
        #    if twitter_stat is not None:
        #        application.db.session.add(twitter_stat)
    #application.db.session.commit()

    """
    price_volumes = PriceService().get_price_for_coins(coins=coins, start_date=datetime.datetime.now() - datetime.timedelta(days=1))
    for price_volume_list in price_volumes:
        for price_volume in price_volume_list:
            insert_update(PriceVolume, price_volume)
    application.db.session.commit()

    application.scheduler.add_job(lambda: GithubService().update_latest_stats(),
                                  trigger='interval',
                                  hours=1,
                                  max_instances=1)
    application.scheduler.add_job(lambda: RedditService().update_latest_stats(),
                                  trigger='interval',
                                  hours=1,
                                  max_instances=1)

    application.scheduler.add_job(lambda: BlockListener().start(), trigger='interval', seconds=1, max_instances=12)
    application.scheduler.add_job(lambda: EthereumBlockService().update_latest_queue(), trigger='interval',
                                  seconds=3, max_instances=1)
    application.scheduler.add_job(lambda: EosBlockService().update_latest_queue(), trigger='interval',
                                  seconds=3, max_instances=1)
    """
    application.run()

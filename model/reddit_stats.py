from sqlalchemy.orm import relationship

from app import db


class RedditStats(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    subscribers = db.Column(db.Integer, nullable=False)
    date = db.Column(db.Date, nullable=False)
    date_time = db.Column(db.DateTime, nullable=False)

    coin_id = db.Column(db.BigInteger, db.ForeignKey('coin.id'))
    coin = relationship("Coin")

from __future__ import annotations

from app import db


class Coin(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    token_name = db.Column(db.String(32), nullable=False)
    description = db.Column(db.String(4096))
    ticker_symbol = db.Column(db.String(32))
    company_name = db.Column(db.String(64))
    industry = db.Column(db.String(64))
    ico = db.Column(db.Boolean())
    mineable = db.Column(db.Boolean())
    open_source = db.Column(db.Boolean())
    ico_raise = db.Column(db.BigInteger())
    max_supply = db.Column(db.BigInteger())
    hash_algorithm = db.Column(db.String(64))
    block_chain_type = db.Column(db.String(64))
    execution = db.Column(db.Integer())
    potential = db.Column(db.Integer())
    market_cap_cluster = db.Column(db.String(16))
    age_cluster = db.Column(db.String(16))
    disclosure = db.Column(db.Boolean())
    website_url = db.Column(db.String(256))
    white_paper_url = db.Column(db.String(512))
    twitter = db.Column(db.String(128))
    reddit = db.Column(db.String(128))
    telegram = db.Column(db.String(128))
    github = db.Column(db.String(128))
    contract_address = db.Column(db.String(128))
    deleted = db.Column(db.Boolean())

    @classmethod
    def all(cls):
        return cls.query.all()

    def update_from_remote(self, coin: Coin):
        self.token_name = coin.token_name
        self.description = coin.description
        self.ticker_symbol = coin.ticker_symbol
        self.company_name = coin.company_name
        self.industry = coin.industry
        self.ico = coin.ico
        self.mineable = coin.mineable
        self.open_source = coin.open_source
        self.ico_raise = coin.ico_raise
        self.max_supply = coin.max_supply
        self.hash_algorithm = coin.hash_algorithm
        self.block_chain_type = coin.block_chain_type
        self.execution = coin.execution
        self.potential = coin.potential
        # intentionally left out from remote ->
        # market_cap_cluster
        # age_cluster
        self.disclosure = coin.disclosure
        self.website_url = coin.website_url
        self.white_paper_url = coin.white_paper_url
        self.twitter = coin.twitter
        self.reddit = coin.reddit
        self.telegram = coin.telegram
        self.contract_address = coin.contract_address
        self.deleted = coin.deleted

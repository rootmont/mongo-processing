from sqlalchemy.orm import relationship

from app import db


class PriceVolume(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    price_high = db.Column(db.Numeric(asdecimal=False))
    price_low = db.Column(db.Numeric(asdecimal=False))
    price_open = db.Column(db.Numeric(asdecimal=False))
    price_close = db.Column(db.Numeric(asdecimal=False))
    trading_volume = db.Column(db.Integer)
    date = db.Column(db.Date, nullable=False)
    coin_id = db.Column(db.BigInteger, db.ForeignKey('coin.id'))
    coin = relationship("Coin")
    ticker_symbol = db.Column(db.String(32), nullable=False)

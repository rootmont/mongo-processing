from sqlalchemy.orm import relationship

from app import db


class TwitterStats(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    tweets = db.Column(db.Integer, nullable=False)
    following = db.Column(db.Integer, nullable=False)
    followers = db.Column(db.Integer, nullable=False)
    leadership = db.Column(db.Numeric(16, 8, asdecimal=False), nullable=False)
    date = db.Column(db.Date, nullable=False)
    date_time = db.Column(db.DateTime, nullable=False)

    coin_id = db.Column(db.BigInteger, db.ForeignKey('coin.id'))
    coin = relationship("Coin")

from __future__ import annotations
import datetime
from typing import List, Union
from sqlalchemy import desc

from app import db


class BlockTransactions(db.Model):
    def __init__(self, block_chain: str, block_num: int, date_time: datetime.datetime, epoch_second: int, num_transactions: int):
        self.block_chain = block_chain,
        self.block_num = block_num,
        self.date_time = date_time,
        self.epoch_second = epoch_second,
        self.num_transactions = num_transactions

    id = db.Column(db.BigInteger, primary_key=True)
    block_chain = db.Column(db.String(32), nullable=False)
    block_num = db.Column(db.Integer, nullable=False)
    date_time = db.Column(db.DateTime, nullable=False)
    epoch_second = db.Column(db.Integer, nullable=False)
    num_transactions = db.Column(db.Integer, nullable=False)

    @classmethod
    def delete_block(cls, block_chain: str, block_num: int) -> None:
        db.session.query(cls).filter(cls.block_chain == block_chain).filter(cls.block_num == block_num).delete()
        # commit here -> thinking about this still.
        db.session.commit()

    @classmethod
    def get_blocks_by_date(cls, block_chain: str, start_date: datetime.datetime) -> List[BlockTransactions]:
        block_chain = block_chain.lower()
        return db.session.query(cls).filter(cls.block_chain == block_chain).filter(cls.date_time >= start_date)

    @classmethod
    def get_blocks(cls, block_chain: str, start_num: int, end_num: int) -> List[BlockTransactions]:
        block_chain = block_chain.lower()
        return db.session.query(cls).filter(cls.block_chain == block_chain).filter(cls.block_num >= start_num)\
            .filter(cls.block_num <= end_num)

    @classmethod
    def get_latest_block(cls, block_chain: str) -> BlockTransactions:
        block_chain = block_chain.lower()
        return db.session.query(cls).filter(cls.block_chain == block_chain).order_by(desc(cls.block_num)).first()

    @classmethod
    def get_block_by_chain_and_num(cls, block_chain: str, block_num: int) -> Union[BlockTransactions, None]:
        return db.session.query(cls).filter(cls.block_num == block_num).filter(cls.block_chain == block_chain).first()

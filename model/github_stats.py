from sqlalchemy.orm import relationship

from app import db


class GithubStats(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    size = db.Column(db.Integer, nullable=False)
    star_gazers = db.Column(db.Integer, nullable=False)
    forks = db.Column(db.Integer, nullable=False)
    open_issues = db.Column(db.Integer, nullable=False)
    last_commit = db.Column(db.DateTime, nullable=False)
    contributors = db.Column(db.Integer, nullable=False)
    contributions = db.Column(db.Integer, nullable=False)
    date = db.Column(db.Date, nullable=False)
    date_time = db.Column(db.DateTime, nullable=False)

    coin_id = db.Column(db.BigInteger, db.ForeignKey('coin.id'))
    coin = relationship("Coin")

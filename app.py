from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from apscheduler.schedulers.background import BackgroundScheduler

from config import *


class AlchemyModded(SQLAlchemy):
    def apply_pool_defaults(self, app, options):
        super().apply_pool_defaults(app, options)
        options["pool_pre_ping"] = True


class Application:
    def __init__(self):
        self._scheduler = BackgroundScheduler()

        self._app = Flask(__name__)
        # self._app.debug = bool(self._config['debug'])
        self._app.debug = True
        self._app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://' + DB_USER + ':' + DB_PASSWORD + '@' + \
                                                      DB_LOCATION + ':' + DB_PORT + '/' + DB_NAME
        self._app.config['SQLALCHEMY_POOL_SIZE'] = 16
        self._app.config['SQLALCHEMY_RECORD_QUERIES'] = True
        self._app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        self._db = AlchemyModded(self._app)

    def run(self):
        self._scheduler.start()
        self._app.run(host="0.0.0.0", port=5000)

    @property
    def flask_app(self):
        return self._app

    @property
    def db(self):
        return self._db

    @property
    def scheduler(self):
        return self._scheduler


application = Application()
db = application.db

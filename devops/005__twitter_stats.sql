USE api_stats;

DROP TABLE IF EXISTS `twitter_stats`;

CREATE TABLE `twitter_stats` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tweets` int(11) NOT NULL,
  `following` int(11) NOT NULL,
  `followers` int(11) NOT NULL,
  `leadership` decimal(16,8) NOT NULL,
  `date` date NOT NULL,
  `date_time` timestamp NOT NULL,
  `coin_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `coin_id` (`coin_id`),
  CONSTRAINT `twitter_stats_ibfk_1` FOREIGN KEY (`coin_id`) REFERENCES `coin` (`id`)
) ENGINE=InnoDB;

CREATE INDEX idx_coin_id_date ON twitter_stats(coin_id, date);
CREATE INDEX idx_coin_id_date_time ON twitter_stats(coin_id, date_time);

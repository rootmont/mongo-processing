USE api_stats;

DROP TABLE IF EXISTS `block_transactions`;

CREATE TABLE `block_transactions` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `block_chain` varchar(32) NOT NULL,
  `block_num` int(11) NOT NULL,
  `date_time` datetime NOT NULL,
  `epoch_second` int(11) NOT NULL,
  `num_transactions` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;


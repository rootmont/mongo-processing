USE api_stats;

DROP TABLE IF EXISTS `coin`;

CREATE TABLE `coin` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `token_name` varchar(32) NOT NULL,
  `description` varchar(4096) DEFAULT NULL,
  `ticker_symbol` varchar(32) DEFAULT NULL,
  `company_name` varchar(64) DEFAULT NULL,
  `ico` tinyint(1) DEFAULT NULL,
  `mineable` tinyint(1) DEFAULT NULL,
  `open_source` tinyint(1) DEFAULT NULL,
  `ico_raise` bigint(20) DEFAULT NULL,
  `max_supply` bigint(20) DEFAULT NULL,
  `hash_algorithm` varchar(64) DEFAULT NULL,
  `block_chain_type` varchar(64) DEFAULT NULL,
  `execution` int(11) DEFAULT NULL,
  `potential` int(11) DEFAULT NULL,
  `market_cap_cluster` varchar(16) DEFAULT NULL,
  `age_cluster` varchar(16) DEFAULT NULL,
  `disclosure` tinyint(1) DEFAULT NULL,
  `website_url` varchar(256) DEFAULT NULL,
  `white_paper_url` varchar(512) DEFAULT NULL,
  `twitter` varchar(128) DEFAULT NULL,
  `reddit` varchar(128) DEFAULT NULL,
  `telegram` varchar(128) DEFAULT NULL,
  `contract_address` varchar(128) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT FALSE,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;


ALTER TABLE coin ADD COLUMN github VARCHAR(128) AFTER telegram;

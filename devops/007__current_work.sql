USE api_stats;

DROP TABLE IF EXISTS `current_work`;

CREATE TABLE `current_work` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `work_name` varchar(32) NOT NULL,
  `data` text NOT NULL,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2;

USE api_stats;

DROP TABLE IF EXISTS `github_stats`;

CREATE TABLE `github_stats` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `size` int(11) NOT NULL,
  `star_gazers` int(11) NOT NULL,
  `forks` int(11) NOT NULL,
  `open_issues` int(11) NOT NULL,
  `last_commit` datetime NOT NULL,
  `contributors` int(11) NOT NULL,
  `contributions` int(11) NOT NULL,
  `date` date NOT NULL,
  `date_time` datetime NOT NULL,
  `coin_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `coin_id` (`coin_id`),
  CONSTRAINT `github_stats_ibfk_1` FOREIGN KEY (`coin_id`) REFERENCES `coin` (`id`)
) ENGINE=InnoDB;

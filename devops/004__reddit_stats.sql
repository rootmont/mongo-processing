USE api_stats;

DROP TABLE IF EXISTS `reddit_stats`;

CREATE TABLE `reddit_stats` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `subscribers` int(11) NOT NULL,
  `coin_id` bigint(20) DEFAULT NULL,
  `date` date NOT NULL,
  `date_time` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  KEY `coin_id` (`coin_id`),
  CONSTRAINT `reddit_stats_ibfk_1` FOREIGN KEY (`coin_id`) REFERENCES `coin` (`id`)
) ENGINE=InnoDB;


CREATE INDEX idx_coin_id_date ON reddit_stats(coin_id, date);
CREATE INDEX idx_coin_id_date_time ON reddit_stats(coin_id, date_time);
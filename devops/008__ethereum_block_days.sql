use api_stats;

CREATE TABLE `ethereum_block_days` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `start_block` int(11) NOT NULL,
  `end_block` int(11) NOT NULL,
  `day` date NOT NULL,
  `blocks` LONGTEXT DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `day` (`day`)
) ENGINE=InnoDB;

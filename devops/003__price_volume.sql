USE api_stats;

DROP TABLE IF EXISTS `price_volume`;

CREATE TABLE `price_volume` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `price_high` decimal(16,7) DEFAULT NULL,
  `price_low` decimal(16,7) DEFAULT NULL,
  `price_open` decimal(16,7) DEFAULT NULL,
  `price_close` decimal(16,7) DEFAULT NULL,
  `trading_volume` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `coin_id` bigint(20) DEFAULT NULL,
  `ticker_symbol` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `coin_id` (`coin_id`),
  CONSTRAINT `price_volume_ibfk_1` FOREIGN KEY (`coin_id`) REFERENCES `coin` (`id`)
) ENGINE=InnoDB;


ALTER TABLE `price_volume` ADD UNIQUE `unique_index`(`coin_id`, `date`);
